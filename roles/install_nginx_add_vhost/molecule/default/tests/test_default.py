import os
import testinfra.utils.ansible_runner
import pytest


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('svc', [
    ('nginx')
])
def test_nginx_installed(host, svc):
    package = host.package(svc)
    assert package.is_installed


@pytest.mark.parametrize('srvc', [
    ('nginx')
])
def test_nginx_running(host, srvc):
    service = host.service(srvc)
    assert service.is_running


@pytest.mark.parametrize('protocol,port', [
    ('tcp', '80'),
    ('tcp', '443'),
])
def test_listening_socket(host, protocol, port):
    socket = host.socket('%s://0.0.0.0:%s' % (protocol, port))
    assert socket.is_listening
