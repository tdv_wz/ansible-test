[![pipeline status](https://gitlab.com/tdv_wz/ansible-test/badges/master/pipeline.svg)](https://gitlab.com/tdv_wz/ansible-test/commits/master)

## Ansible роль
 - `hosts.cfg` - список хостов
 - `group_vars/web` - ssh пользователь

Для запуска роли вручную нужно перейти в директорию с проектом и выполнить
```bash
$ ansible-playbook install-nginx.yml
```
 - Nginx будет установлен, добавлен в автозапуск и запущен
 - файл виртуального хоста (example.com) будет расположен в директории /etc/nginx/conf.sc/
 - example.com будет доступен по https, self-signed сертификат будет создан с помощью Ansible

## Тестирование molecule + testinfra (centos 7)

Тестируются:
 - `nginx установлен`
 - `nginx запущен`
 - `Порт 80 принимает соединения`
 - `Порт 443 принимает соединения`

Для запуска тестов вручную необходимо перейти в директорию роли и запустить тест
```bash
$ cd roles/install_nginx_add_vhost/
$ molecule test
```
в таком случае последовательно выполнятся шаги, которые можно выполнить по отдельности, например *lint*, *create*, *prepare*, *converge*, *idempotence*, *verify* (тот самый тест), *destroy*.

## При внесении любых изменений в проект, выполняется запуск CentOS 7 Systemd из-под docker dind в шаред раннере и тестирование роли по шагам, описанным пунктом выше.
